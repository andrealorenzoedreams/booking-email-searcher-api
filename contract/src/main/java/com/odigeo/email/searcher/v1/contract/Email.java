package com.odigeo.email.searcher.v1.contract;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Email {

    private String subject;
    private String body;
    private EmailType emailType;
    private Long sentEpochMilli;

    public Email() {
    }

    public Email(String subject, String body, EmailType emailType, Long sentEpochMilli) {
        this.subject = subject;
        this.body = body;
        this.emailType = emailType;
        this.sentEpochMilli = sentEpochMilli;
    }

    public String getSubject() {
        return subject;
    }

    public Email setSubject(String subject) {
        this.subject = subject;
        return this;
    }

    public String getBody() {
        return body;
    }

    public Email setBody(String body) {
        this.body = body;
        return this;
    }

    public EmailType getEmailType() {
        return emailType;
    }

    public Email setEmailType(EmailType emailType) {
        this.emailType = emailType;
        return this;
    }

    public Long getSentEpochMilli() {
        return sentEpochMilli;
    }

    public Email setSentEpochMilli(Long sentEpochMilli) {
        this.sentEpochMilli = sentEpochMilli;
        return this;
    }
}
