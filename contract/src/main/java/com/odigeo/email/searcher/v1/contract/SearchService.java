package com.odigeo.email.searcher.v1.contract;

import javax.ws.rs.GET;
import javax.ws.rs.MatrixParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.rmi.RemoteException;
import java.util.List;

@Path("service/v1")
@Produces(MediaType.APPLICATION_JSON)
public interface SearchService {

    @GET
    @Path("/search/{bookingId}")
    List<Email> search(@PathParam("bookingId") long bookingId) throws RemoteException;

    @GET
    @Path("/search")
    List<Email> searchBetweenPeriod(@MatrixParam("init") long init, @MatrixParam("end") long end) throws RemoteException;

}
