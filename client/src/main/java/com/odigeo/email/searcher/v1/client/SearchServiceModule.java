package com.odigeo.email.searcher.v1.client;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;
import com.odigeo.commons.rest.guice.AbstractRestUtilsModule;
import com.odigeo.commons.rest.guice.configuration.ServiceConfiguration;
import com.odigeo.email.searcher.v1.contract.SearchService;

public class SearchServiceModule extends AbstractRestUtilsModule<SearchService> {

    private final ConnectionConfiguration connectionConfiguration;

    SearchServiceModule(ConnectionConfiguration connectionConfiguration, ServiceNotificator... serviceNotificators) {
        super(SearchService.class, serviceNotificators);
        this.connectionConfiguration = connectionConfiguration;
    }

    @Override
    protected ServiceConfiguration<SearchService> getServiceConfiguration(Class<SearchService> serviceContractClass) {
        final ServiceConfiguration.Builder<SearchService> builder = new ServiceConfiguration.Builder<>(serviceContractClass)
                .withConnectionConfiguration(connectionConfiguration);
        return builder.build();
    }
}
