package com.odigeo.email.searcher.v1.client;

import com.odigeo.commons.rest.ServiceNotificator;
import com.odigeo.commons.rest.configuration.ConnectionConfiguration;

public class SearchServiceModuleBuilder {

    private static final int DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAULT_SOCKET_TIMEOUT_IN_MILLIS = 60 * 1000;
    private static final int DEFAUTL_POOL_SIZE = 50;

    private int connectionTimeoutInMillis;
    private int socketTimeoutInMillis;
    private int maxConcurrentConnections;

    public SearchServiceModuleBuilder() {
        connectionTimeoutInMillis = DEFAULT_CONNECTION_TIMEOUT_IN_MILLIS;
        socketTimeoutInMillis = DEFAULT_SOCKET_TIMEOUT_IN_MILLIS;
        maxConcurrentConnections = DEFAUTL_POOL_SIZE;
    }

    public SearchServiceModuleBuilder withConnectionTimeoutInMillis(int connectionTimeoutInMillis) {
        this.connectionTimeoutInMillis = connectionTimeoutInMillis;
        return this;
    }

    public SearchServiceModuleBuilder withSocketTimeoutInMillis(int socketTimeoutInMillis) {
        this.socketTimeoutInMillis = socketTimeoutInMillis;
        return this;
    }

    public SearchServiceModuleBuilder withMaxConcurrentConnections(int maxConcurrentConnections) {
        this.maxConcurrentConnections = maxConcurrentConnections;
        return this;
    }

    public SearchServiceModule build(ServiceNotificator... serviceNotificators) {
        ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration();
        connectionConfiguration.setConnectionTimeoutInMillis(connectionTimeoutInMillis);
        connectionConfiguration.setMaxConcurrentConnections(maxConcurrentConnections);
        connectionConfiguration.setSocketTimeoutInMillis(socketTimeoutInMillis);
        return new SearchServiceModule(connectionConfiguration, serviceNotificators);
    }
}
